# Gabarits des services web pré-configurés pour Easy-Pi-Hosting

Ce dépôt héberge les gabarits officiels des services web pré-empaqueté pour le projet "Héberges Ta Vie".  Pour les utiliser il faut avoir un Portainer d'installer et d'activer les gabarits externes (Use custom templates) et d'entrer l'une des URL suivante dans le champs correspondant :

- si vous utilisez un PC (Debian avec Docker)

https://framagit.org/linuq/easy-pi-hosting-template/raw/master/templates.pc.json

- si vous avez un Raspberry Pi (Hypriot)

https://framagit.org/linuq/easy-pi-hosting-template/raw/master/templates.pi.json

Chacune des piles offertes est basée sur les images Docker officielles ou sont des adaptations à partir de la version officielle la plus proche (surtout pour ARM).  Ceci permettra à moyen terme d'ajouter des activateurs sur modification d'un dépôt parent pour déclencher la mise à jours en cascade.  De plus les variables pour contrôler Traefik ont été exposées comme variables d'environnement et les gabarits ont été adaptés pour renseigner ces champs.

En plus des images officielles, après avoir vérifier leur procédure de création des images, j'ajoute les images de [LinuxServer](https://www.linuxserver.io/our-images) comme base pour les piles logicielles.

Si une pile peux être déployé en passant des paramètres à la pile en lieux et place d'une partie de son installation, cette solution sera retenue pour permettre au conteneur de pouvoir être le plus éphémère possible (The Docker Way).

## Gabarits fonctionnels (besoin de test)

### CMS

* Dokuwiki (basée sur `xtremxpert/rpi-dokuwiki`)
* WordPress (basé sur `wordpress:latest` avec `xtremxpert/rpi-mariadb` et `adminer:latest`)

### Gallerie

* Lychee (basée sur `lsioarmhf/lychee` avec `xtremxpert/rpi-mariadb` et `adminer:latest`)
* PhotoShow (basée sur `lsioarmhf/photoshow`)
* Piwigo (basée sur `lsioarmhf/piwigo` avec `xtremxpert/rpi-mariadb` et `adminer:latest`)

### Partage

* NextCloud (basée sur `nextcloud:13` avec  `postgres:10-alpine`)

### ERP

* Odoo (basée sur `xtremxpert/rpi-odoo` avec  `postgres:9-alpine` et `xtremxpert/rpi-phppgadmin:alpine`)

## Autre gabarits prévus

* Joomla (basé sur `joomla:latest`)
* Drupal (basé sur `drupal:latest`)
* Pi-Hole
* Rasp-AP
